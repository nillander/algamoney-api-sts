CREATE TABLE categoria(
    id bigint PRIMARY KEY AUTO_INCREMENT,
    nome varchar(50) NOT NULL

) ENGINE=InnoDB DEFAULT CHARSET=UTF8MB4;

INSERT INTO categoria values (null, 'Lazer');
INSERT INTO categoria values (null, 'Alimentação');
INSERT INTO categoria values (null, 'Supermercado');
INSERT INTO categoria values (null, 'Farmácia');
INSERT INTO categoria values (null, 'Outros');