package com.algaworks.security.util;

import java.util.UUID;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("gerador")
public class GeradorSenha {

    @PostMapping("/secret")
    public static String gerarSenha(@RequestBody String secret) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        // System.out.println(encoder.encode("@ngul@r0"));
        return encoder.encode(secret);
    }

    @GetMapping("/uuid")
    public static String geraStringUUID() {
        UUID uuid = UUID.randomUUID();
        String uuidAsString = uuid.toString();
        return uuidAsString;
    }

}
