package com.algaworks.handlers;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.algaworks.exceptionhandler.AlgamoneyExceptionHandler.Erro;
import com.algaworks.service.exception.PessoaInativaException;
import com.algaworks.service.exception.PessoaInexistenteException;

/**
 * Permite declarações de anotação mais concisas — por
 * exemplo, @ControllerAdvice("org.my.pkg") é equivalente
 * a @ControllerAdvice(basePackages = "org.my.pkg").
 * #
 * Os controladores que pertencem a esses pacotes básicos ou seus subpacotes
 * irão
 * ser incluído — por exemplo, @ControllerAdvice(basePackages = "org.my.pkg")
 * ou @ControllerAdvice(basePackages = {"org.my.pkg", "org.my.other.pkg"}).
 */
/* Package por String */
// @ControllerAdvice({"com.algaworks.pacote_1", "com.algaworks.pacote_2"})
/* Package por Referência de Classe */
// @ControllerAdvice(basePackageClasses={Controller_1.class,Controller_2.class})
/* Referência por Classes Específicas */
// @ControllerAdvice(assignableTypes={Controller_1.class,Controller_2.class})
/* Para Toda a Aplicação */
@ControllerAdvice
public class LancamentoHandler {

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler({ PessoaInexistenteException.class })
    public ResponseEntity<Object> handlePessoaInexistente(PessoaInexistenteException ex) {
        String mensagemUsuario = messageSource.getMessage("pessoa.inexistente", null,
                LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.getCause() != null ? ex.getCause().toString() : ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return ResponseEntity.badRequest().body(erros);
    }

    @ExceptionHandler({ PessoaInativaException.class })
    public ResponseEntity<Object> handlePessoaInativaException(PessoaInativaException ex) {
        String mensagemUsuario = messageSource.getMessage("pessoa.inativa", null,
                LocaleContextHolder.getLocale());
        String mensagemDesenvolvedor = ex.getCause() != null ? ex.getCause().toString() : ex.toString();
        List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
        return ResponseEntity.badRequest().body(erros);
    }

}
