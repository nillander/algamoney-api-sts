package com.algaworks.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.algaworks.model.Pessoa;
import com.algaworks.repository.PessoaRepository;
import com.algaworks.service.exception.PessoaInativaException;
import com.algaworks.service.exception.PessoaInexistenteException;

@Service
public class PessoaService {

	@Autowired
	private PessoaRepository pessoaRepository;

	public Pessoa buscarPeloId(Long id) {
		return pessoaRepository.findById(id)
				.orElseThrow(() -> new EmptyResultDataAccessException("Pessoa não encontrada!", 1));
	}

	public Pessoa atualizar(Long id, Pessoa pessoa) {
		Pessoa pessoaSalva = buscarPeloId(id);
		BeanUtils.copyProperties(pessoa, pessoaSalva, "id");
		return pessoaRepository.save(pessoaSalva);
	}

	public void atualizarPropriedadeAtivo(Long id, Boolean status) {
		Pessoa pessoaSalva = buscarPeloId(id);
		pessoaSalva.setAtivo(status);
		pessoaRepository.save(pessoaSalva);
	}

	public Pessoa getPessoaAtivaPeloId(Long id) {
		final Pessoa pessoa = pessoaRepository.findById(id)
				.orElseThrow(() -> new PessoaInexistenteException());
		verificarSePessoaAtiva(pessoa);
		return pessoa;
	}

	public void verificarSePessoaAtiva(final Pessoa pessoa) {
		if (pessoa.isInativo()) {
			throw new PessoaInativaException();
		}
	}

}
