package com.algaworks.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.algaworks.model.Lancamento;
import com.algaworks.model.Pessoa;
import com.algaworks.repository.LancamentoRepository;
import com.algaworks.repository.PessoaRepository;
import com.algaworks.service.exception.PessoaInativaException;
import com.algaworks.service.exception.PessoaInexistenteException;

@Service
public class LancamentoService {

	@Autowired
	private PessoaService pessoaService;

	@Autowired
	private CategoriaService categoriaService;

	@Autowired
	private LancamentoRepository lancamentoRepository;

	@Autowired
	private PessoaRepository pessoaRepository;

	public Lancamento salvar(Lancamento lancamento) {
		Pessoa pessoa = pessoaRepository.findById(lancamento.getPessoa().getId())
				.orElseThrow(() -> new PessoaInexistenteException());
		if (pessoa.isInativo()) {
			throw new PessoaInativaException();
		}
		return lancamentoRepository.save(lancamento);
	}

	public Lancamento buscarPeloId(Long id) {
		return lancamentoRepository.findById(id)
				.orElseThrow(() -> new EmptyResultDataAccessException("Lançamento não encontrado!", 1));
	}

	public Lancamento atualizar(Long id, Lancamento lancamento) {
		Lancamento lancamentoRecuperado = buscarPeloId(id);
		pessoaService.getPessoaAtivaPeloId(lancamento.getPessoa().getId());
		categoriaService.getCategoriaPeloId(lancamento.getCategoria().getId());

		BeanUtils.copyProperties(lancamento, lancamentoRecuperado, "id");
		return lancamentoRepository.save(lancamentoRecuperado);
	}

}
