package com.algaworks.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.algaworks.model.Categoria;
import com.algaworks.repository.CategoriaRepository;

@Service
public class CategoriaService {

    @Autowired
    private CategoriaRepository categoriaRepository;

    public Categoria getCategoriaPeloId(Long id) {
        return categoriaRepository.findById(id)
                .orElseThrow(() -> new EmptyResultDataAccessException("Categoria não encontrada!", 1));
    }

}
