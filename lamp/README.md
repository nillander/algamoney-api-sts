![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)

#  Estrutura LAMP construída com Docker Compose
  
![Landing Page](https://user-images.githubusercontent.com/43859895/141092846-905eae39-0169-4fd7-911f-9ff32c48b7e8.png)
  

Um ambiente de estrutura LAMP básico criado usando o Docker Compose.

Consiste no seguinte:
* PHP
* Apache
* MySQL
* phpMyAdmin
* Redis

A partir de agora, temos várias versões diferentes do PHP. Use a versão php apropriada conforme necessário:

* 5.4.x
* 5.6.x
* 7.1.x
* 7.2.x
* 7.3.x
* 7.4.x
> Observe que simplificamos a estrutura do projeto de várias ramificações para cada versão do php, para uma ramificação principal centralizada. Por favor, deixe-nos saber se você encontrar quaisquer problemas. 
##  Instalação
  
* Clone este repositório diretamente dentro da pasta do novo projeto
* Configure .env como necessário
* Execute o comando `docker-compose up -d`.

```shell
git clone git@gitlab.goinfra.go.gov.br:desenvolvimento/lamp.git
cd lamp
rm -rfv .git
# copie o arquivo "sample.env" para ".env" e modifique como precisar
cp sample.env .env
# execute para subir a estrutura da aplicação
docker-compose up -d
# acesse http://localhost/ para visitar a aplicação
# e http://localhost:9191/ para visitar o administrador de banco de dados
# e para ver a página de teste do lamp acesse http://localhost/lamp/
```
Sua estrutura LAMP agora está pronta!! Você pode acessá-lo através `http://localhost`.

##  Configuração e Uso


### Informações gerais 

Esta estrutura Docker é construído para desenvolvimento local e não para uso em produção.

### Configuração

Este pacote vem com opções de configuração padrão. Você pode modificá-los criando o arquivo `.env` em seu diretório raiz.

Para facilitar, basta copiar o conteúdo do arquivo `sample.env` e atualizar os valores das variáveis de ambiente conforme sua necessidade.

### Variáveis de Configuração

Existem as seguintes variáveis de configuração disponíveis e você pode personalizá-las substituindo-as em seu próprio arquivo `.env.`

---
#### PHP
---
_**PHPVERSION**_

É usado para especificar qual versão do PHP você deseja usar. Padrões sempre para a última versão do PHP. 

_**PHP_INI**_

Defina sua modificação `php.ini` personalizada para atender às suas necessidades. 

---
#### Apache 
---

_**DOCUMENT_ROOT**_

É uma raiz de documento para o servidor Apache. O valor padrão para isso é `./www`. Todos os seus sites irão aqui e serão sincronizados automaticamente.

_**VHOSTS_DIR**_

Isso é para hosts virtuais. O valor padrão para isso é `./config/vhosts`. Você pode colocar seus arquivos conf de hosts virtuais aqui.

> Certifique-se de adicionar uma entrada ao arquivo `hosts` do seu sistema para cada host virtual.

_**APACHE_LOG_DIR**_

Isso será usado para armazenar logs do Apache. O valor padrão para isso é `./logs/apache2`.


---
#### Database
---

_**DATABASE**_

Defina qual versão do MySQL ou MariaDB você gostaria de usar. 

_**MYSQL_DATA_DIR**_

Este é o diretório de dados do MySQL. O valor padrão para isso é `./data/mysql`. Todos os seus arquivos de dados MySQL serão armazenados aqui.

_**MYSQL_LOG_DIR**_

Isso será usado para armazenar logs do Apache. O valor padrão para isso é `./logs/mysql`.

## Web Server

O Apache está configurado para rodar na porta 80. Então, você pode acessá-lo via `http://localhost`.

#### Apache Modules

Por padrão, os seguintes módulos estão ativados.

* rewrite
* headers

> Se você quiser habilitar mais módulos, basta atualizar `./bin/webserver/Dockerfile`. Você também pode gerar um PR e iremos mesclar se parecer bom para uso geral.
> 
> Você tem que reconstruir a imagem do docker executando `docker-compose build` e reinicie os contêineres docker.

#### Conectar via SSH

Você pode se conectar ao servidor web usando o comando `docker-compose exec` para executar várias operações nele. Use o comando abaixo para fazer login no contêiner via ssh.

```shell
docker-compose exec webserver bash
```

## PHP

A versão instalada depende do seu arquivo `.env`.

#### Extensões

Por padrão, as seguintes extensões são instaladas.

Pode diferir para versões do PHP <7.x.x
* mysqli
* pdo_sqlite
* pdo_mysql
* mbstring
* zip
* intl
* mcrypt
* curl
* json
* iconv
* xml
* xmlrpc
* gd

> Se você deseja instalar mais extensões, basta atualizar `./bin/webserver/Dockerfile`. Você também pode gerar um PR e mesclaremos se parecer bom para uso geral.
>
>
> Você precisa reconstruir a imagem do docker executando `docker-compose build` e reiniciar os contêineres do docker.

## phpMyAdmin

phpMyAdmin está configurado para rodar na porta 9191. Use as seguintes credenciais padrão.

http://localhost:9191/

username: root 

password: approotpass

## Redis

Ele vem com Redis inativo, se ativo, rodando porta padrão `6379`.

## Contribuindo

Ficaremos felizes se você quiser criar uma solicitação pull ou ajudar as pessoas com seus problemas. Se você deseja criar um PR, lembre-se de que essa estrutura não foi criada para uso em produção e as alterações devem ser boas para fins gerais e não superespecializadas.

> Observe que simplificamos a estrutura do projeto de várias ramificações para cada versão do php, para uma ramificação principal centralizada. Por favor, crie seu PR contra o branch master. 

Obrigado! 

## Por que você não deve usar esta estrutura não modificada na produção

Queremos capacitar os desenvolvedores a criar aplicativos criativos rapidamente. Portanto, estamos fornecendo um ambiente de desenvolvimento local fácil de configurar para vários frameworks e versões PHP diferentes.

Em Produção você deve modificar no mínimo os seguintes assuntos:

* php handler: mod_php=> php-fpm
* secure mysql users: com limitações de IP de origem adequadas

## Referências

[Nillander](https://sgpld.com.br/)

[harshalone](https://github.com/harshalone/docker-compose-lamp)

[SprintCube](https://github.com/sprintcube/docker-compose-lamp)
